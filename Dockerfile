# sdss/data_docker

FROM ubuntu:18.04
MAINTAINER Joel Brownstein <joelbrownstein@astro.utah.edu>
LABEL build="Ubuntu 18.04 with nginx-extras (Sep 2, 2019 2:53MDT)"
ARG DEBIAN_FRONTEND=noninteractive

# ========================================================
# WEBSERVER> nginx-extras
# ========================================================

RUN apt-get update \
    && apt-get install -y nginx-extras

# ========================================================
# USERS > admin
# ========================================================

RUN apt-get update \
    && apt-get install -y vim sudo rsyslog subversion lmod tclsh build-essential openssh-server \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/bin
ADD users/admin/tmp/bin /tmp/bin/
RUN chmod +x /tmp/bin/*
RUN mkdir -p /var/run/sshd && sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config \
    && sed -i 's/PermitRootLogin without-password/PermitRootLogin no/' /etc/ssh/sshd_config \
    && sed -ri 's/^session\s+required\s+pam_loginuid.so$/session optional pam_loginuid.so/' /etc/pam.d/cron \
    && sed -ri 's/^session\s+required\s+pam_loginuid.so$/session optional pam_loginuid.so/' /etc/pam.d/sshd \
    && ln -s /usr/share/lmod/lmod/init/env_modules_python.py /usr/share/lmod/lmod/init/python \
    && ln -s posix_c.so /usr/lib/x86_64-linux-gnu/lua/5.2/posix.so \
    && touch /root/.Xauthority \
    && true

# Add Users and Groups
RUN groupadd sdss
RUN useradd -g sdss sdssadmin \
    && chsh -s /bin/bash sdssadmin \
    && passwd -d sdssadmin \
    && mkdir /home/sdssadmin \
    && addgroup sdssadmin sdss \
    && addgroup sdssadmin staff \
    && true

ADD users/admin/etc/motd /etc/motd
ADD users/admin/home/sdssadmin /home/sdssadmin/
RUN chmod -x /etc/update-motd.d/* \
    && chmod +x /home/sdssadmin/crontab/svn/* \
#    && echo "sdssadmin       ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD: /usr/bin/rsync --daemon" >> /etc/sudoers \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD: /etc/init.d/xinetd *" >> /etc/sudoers \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD: /usr/sbin/service nginx *" >> /etc/sudoers \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD: /usr/sbin/service cron *" >> /etc/sudoers \
    && echo "sdssadmin       ALL=(ALL) NOPASSWD: /usr/sbin/service rsyslog *" >> /etc/sudoers


ENV SSH_KEY ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHQ/qcYdnnlKUy295NZQBrq3kiv4u34W+8AEPRe35/RppAy/wYmaTMK0oE7NXFnatxA+7sFz8BOfHI4si5MHt/wn8tgB8XteSMmYFgLi9BExMlx+7q9jt6tMzUy7gYdx6sh9KCS5hoH8HSt2E5MoZK57UqUMMMaBfcWvBYyt0y+WXujTN1AliAgnI3VdywZP8/k4UQ4K+FwTYSOt3bSzmtD/YdD8ngk+2DSpjASl/1Z16ESz6jYJxqvBhdgT81Q6+Bp8/K8NQrXfKJHNWW1P3yTe5m+DI7LvDu8Bae3fSMvuq4/Vce0Et1HTEvVebCinPO0xmrg3j9LvFtqvVjZjvl joel@dhn

# ========================================================
# HOST > data.sdss.org
# ========================================================

RUN apt-get update \
    && apt-get install -y xinetd rsync \
    && apt-get install -y software-properties-common \
    && apt-get install -y python2.7 \
    && apt-get install -y locales \
	&& rm -rf /var/lib/apt/lists/*

ADD host/data/home/www/ /home/www/
RUN rm -r /var/log/nginx \
    && ln -sf /home/sdssadmin/data.sdss.org/log/nginx /var/log/nginx \
    && ln -sf /home/sdssadmin/data.sdss.org/log/rsync /var/log/rsync \
    && chmod -R o+rX /home/www/data.sdss.org/rsync \
    && ln -fs /usr/share/zoneinfo/US/Mountain /etc/localtime \
    && locale-gen en_US.UTF-8

ADD host/data/etc/rsyncd.conf /etc/rsyncd.conf
ADD host/data/etc/timezone /etc/timezone
ADD host/data/etc/xinetd.d/rsync /etc/xinetd.d/rsyncd
ADD host/data/etc/nginx/nginx.conf /etc/nginx/nginx.conf
ADD host/data/etc/nginx/conf.d/data.mirror.sdss.org.conf /etc/nginx/conf.d/data.mirror.sdss.org.conf
ADD host/data/etc/nginx/conf.d/mirror.sdss.org.conf /etc/nginx/conf.d/mirror.sdss.org.conf
ADD host/data/etc/nginx/conf.d/ssl-only.conf /etc/nginx/conf.d/ssl-only.conf

RUN chmod g+w,o+rw /etc/rsyncd.conf \
    && chmod g+w,o+rw /etc/xinetd.d/rsyncd\
    && chmod -R g+w,o+rw /etc/nginx/nginx.conf \
    && chmod -R g+w,o+rw /etc/nginx/conf.d

EXPOSE 22 80 443 873

CMD ["/tmp/bin/start_services"]

