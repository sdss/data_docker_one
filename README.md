build
-----
    make

is equivalent to:

    make data-docker
    docker build -t data_one .

run at nersc
------------
    docker run -d -p sgn-ext-15.nersc.gov:5022:22 \
               -p sgn-ext-15.nersc.gov:80:80 \
               -p sgn-ext-15.nersc.gov:443:443 \
               -p sgn-ext-15.nersc.gov:873:873  \
               -v /etc/httpd/certs:/etc/httpd/certs:ro \
               -v /global/projecta/projectdirs/sdss/data/sdss:/home/www/data.sdss.org/sdss:ro \
               -v /global/projecta/projectdirs/sdss/staging:/home/www/data.sdss.org/staging:rw \
               -v /global/projecta/projectdirs/sdss/docker/data.sdss.org:/home/sdssadmin/data.sdss.org:rw \
               -h data01 -e HOST_DOMAIN="data.mirror.sdss.org" --name data01 sdss4/data_docker_one
    

run on dhn01.sdss.utah.edu
--------------------------
    docker run \
            -p 155.101.29.162:5022:22 \
            -p 155.101.29.162:5080:80 \
            -p 155.101.29.162:5443:443 \
            -p 155.101.29.162:5873:5873 \
            -v /home/sdssadmin/docker/etc/httpd/certs:/etc/httpd/certs:ro \
            -v /home/sdssadmin/data.sdss.org/sdss:/home/www/data.sdss.org/sdss:ro \
            -v /home/sdssadmin/data.sdss.org/staging:/home/www/data.sdss.org/staging:rw \
            -v /home/sdssadmin/data.sdss.org/docker/data.sdss.org:/home/sdssadmin/data.sdss.org:rw \
            -h data01 -e HOST_DOMAIN="dhn01.sdss.utah.edu" --name data01 data_one

run on desktop
--------------------------
    docker run -p 5022:22 -p 5080:80 -p 5443:443 -p 5873:873 \
        -v /Users/joel/Compute/Docker/Docker.xcworkspace/Sites/etc/httpd/certs:/etc/httpd/certs:ro \
        -v /Users/joel/Compute/Docker/Docker.xcworkspace/Sites/data.sdss.org/data/sdss:/home/www/data.sdss.org/sdss:ro \
        -v /Users/joel/Compute/Docker/Docker.xcworkspace/Sites/data.sdss.org/staging:/home/www/data.sdss.org/staging:rw \
        -v /Users/joel/Compute/Docker/Docker.xcworkspace/Sites/data.sdss.org/docker/data.sdss.org:/home/sdssadmin/data.sdss.org:rw \
        -h data01 -e HOST_DOMAIN="0.0.0.0" --name data01 data_one

        previously HOST_DOMAIN="192.168.99.100"
        
inspect image label
-------------------
$  docker inspect -f "{{json .ContainerConfig.Labels }}" data_one
{"build":"Ubuntu 16.04 with nginx-extras (May 4, 2017 21:36MDT)"}
