if [ -e /usr/share/lmod/lmod/init/bash ]; then
    . /usr/share/lmod/lmod/init/bash
    if [ -e /home/sdssadmin/data.sdss.org/software/svn.sdss.org/data/modulefiles ]; then module use /home/sdssadmin/data.sdss.org/software/svn.sdss.org/data/modulefiles; fi
    if [ -e /home/sdssadmin/data.sdss.org/software/svn.sdss.org/repo/modulefiles ]; then module use /home/sdssadmin/data.sdss.org/software/svn.sdss.org/repo/modulefiles; fi
    if [ -e /home/sdssadmin/.modulefiles ]; then module use /home/sdssadmin/.modulefiles; fi
    if [ -e /home/sdssadmin/.modules ]; then source /home/sdssadmin/.modules; fi
    module list
fi
shopt -s expand_aliases