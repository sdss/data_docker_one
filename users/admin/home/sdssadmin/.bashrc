if [ -f ~/.aliases ]; then
	source ~/.aliases
fi

if [ -e /usr/share/lmod/lmod/init/bash ]; then
    . /usr/share/lmod/lmod/init/bash
    if [ -e /home/sdssadmin/data.sdss.org/software/svn.sdss.org/data/modulefiles ]; then module use /home/sdssadmin/data.sdss.org/software/svn.sdss.org/data/modulefiles; fi
    if [ -e /home/sdssadmin/data.sdss.org/software/svn.sdss.org/repo/modulefiles ]; then module use /home/sdssadmin/data.sdss.org/software/svn.sdss.org/repo/modulefiles; fi
    if [ -e ~/.modulefiles ]; then module use ~/.modulefiles; fi
    if [ -e ~/.modules ]; then source ~/.modules; fi
    module list
fi

