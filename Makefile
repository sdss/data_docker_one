default: data_one

data-docker-dev: nginx admin data

nginx:
	docker build -t webserver webserver/nginx/.

nginx-extras:
	docker build -t webserver webserver/nginx-extras/.

admin:
	docker build -t users users/admin/.

data:
	docker build -t host host/data/.

data_one:
	docker build -t data_one .
